#! /usr/bin/ruby
# coding: utf-8
require 'date'

CURRENT=File.expand_path(File.dirname($0))

# check plughw `aplay -l`
APLAY="aplay -D plughw:2,0"

today = Date.today # 今日
case today.wday
when 2,5 # 火曜、金曜
  `#{APLAY} #{CURRENT}/voice/gomi.wav`
end
